﻿/*Alumnos:
 * Franco Del Medico
 * Ismael L. Scorzelli
 * Agustín M. Blanco
 */


using System;
using System.Collections.Generic;

namespace copos
{
    class Program
    {
        static void Main(string[] args)
        {
                //Creo variable de tipo timespan para guardar el tiempo transcurrido desde la creacion del ultimo copo.
            TimeSpan diferencia;

                //Creo dos objetos DateTime para guardar en uno el momento en en el que se creo el ultimo copo y en el otro el tiempo actual del programa. 
            DateTime inicio, actual;

                //Creo un objeto de tipo tecla para guardar cualquier tecla que se presione mientras el programa esta corriendo para poder luego realizar acciones con ella. 
            ConsoleKeyInfo tecla; 

                //Inicializo el objeto de inicio con el tiempo actual. 
            inicio = DateTime.Now; 

                //Guardo en forma de una constante el ancho que va a tener la pantalla.
            const int ancho = 80; 

                //Guardo en forma de una constante el alto que va a tener la pantalla.              
            const int alto = 25; 

                //Creo e inicializo la variable de velocidad. Esta marcara el ritmo con el que se creen los copos. 
            int velocidad = 500; 

                //Creo un objeto de tipo random para poder generar numeros random al momento de decidir en que posicion crear el nuevo copo.
            Random numRandom = new Random(); 

                //Creo una lista bidimensional para llevar mas facilmente un registro de en que posiciones hay copos y que posiciones se encuentran vacias.
            List<List<Copo>> matrizCopos = new List<List<Copo>>(); 
                
                //Recorro la matriz llenandola de nulls ya que al inicio del programa la pantalla se debe encontrar vacia.
            for(int i = 0; i < alto; i++)
            {
                matrizCopos.Add(new List<Copo>());
                for(int z =0; z < ancho; z++)
                {
                    matrizCopos[i].Add(null);
                }
            }

                //Loop infinito para ejecutar el programa principal.
            while(true)
            {
                    //En caso de que alguna tecla se presionada:
                if (Console.KeyAvailable)
                {
                        //Guardo temporalmente la tecla en el objeto tecla.
                    tecla = Console.ReadKey();                                           
                    switch (tecla.Key)
                    {
                            //Si la tecla presionada es la flecha de arriba, y la velocidad a la que se encuentran cayendo los copos es mayor o igual a 6 milisegundos, aumento la velocidad 5 milisegundos. 
                        case ConsoleKey.UpArrow:
                            if(velocidad >= 6)
                            {
                                velocidad -= 5;
                            }
                            break;

                            //Si la tecla presionada es la flecha hacia abajo, reduzco la velocidad 25 milisegundos. 
                        case ConsoleKey.DownArrow:
                            velocidad += 25;
                            break;
                    }                    
                }
                    //Guardo la la fecha actual en el objeto Datetime "actual".
                actual = DateTime.Now;
                    //Calculo la diferencia de tiempo transcurrido entre inicio y acutal.
                diferencia = actual - inicio;
                    //Si la diferencia de tiempo es mayor al tiempo que debe transcurrir para que se cree un nuevo copo, entonces creo un copo, bajo el resto si es posible, y si la ultima fila se encuentra llena, la elimino. 
                if (diferencia.Milliseconds > velocidad)
                {
                        //Genero un numero random que definira en que columna se creara el nuevo copo.
                    int temp_col = numRandom.Next(0, ancho);

                        //Creo un nuevo copo en la primer fila utilizando como columna el valor generado previamente. 
                    matrizCopos[0][temp_col] = new Copo(1, temp_col);   
                        
                        //Borro toda la pantalla antes de actualizarla para que no se queden las posiciones viejas de los copos dibujadas. 
                    Console.Clear();
                        //Llamo al metodo estatico "bajar copos" que se ocupa de recorrer toda la matriz y, para cada copo, si es posible, lo baja una posicion. 
                    bajar_copos(matrizCopos, alto, ancho);
                    //Llamo al metodo estatico "derretir_ultima_fila_si_esta_llena" que se ocupa de corroborar si la ultima fila se encuentra llena, en caso afirmativo, borra todos los copos de esta. 
                    derretir_ultima_fila_si_esta_llena(matrizCopos, alto, ancho);
                        
                        //Recorro toda la matriz dibujando los copos que haya en esta.
                    for (int i = 0; i< alto; i++)
                    {
                        for(int z =0; z< ancho; z++)
                        {
                            if(matrizCopos[i][z] != null)
                            {
                                //Para dibujar el copo, hago uso del metodo de instancia dibujar con el que cuenta la clase Copo.
                                matrizCopos[i][z].dibujar();
                            }
                            
                        }
                    }
                        //Actualizo el objeto DateTime "inicio" para que lleve la fecha y hora a la que se termino de dibujar el ultimo copo y asi poder saber luego cuando ya paso el tiempo requerido para crear un nuevo copo. 
                    inicio = DateTime.Now;                    
                }
            }
        }

            //Metodo estatico Bajar Copos. Copo su nombre lo indica, se encarga de recorrer la pantalla y donde haya un copo, si es posible, bajarlo.
        static void bajar_copos(List<List<Copo>> matriz, int filas, int cols)
        {
            //Recorro la pantalla de abajo para arriba para asegurarme que los copos bajen solo una posicion. 
            for(int i = filas -1; i >0; i--)
            {
                for(int z=0; z< cols; z++)
                {
                        //Si la posicion actual esta vacia, y en la de arriba hay un copo, bajo el copo de la posicion de arriba a la posicion actual. 
                    if (matriz[i-1][z] != null && matriz[i][z] == null)
                    {
                        matriz[i][z] = matriz[i - 1][z];
                        matriz[i - 1][z] = null;
                            //Actualizo el atributo fila del copo que acabo de mover.
                        matriz[i][z].fila = i;         
                    }
                }
            }
        }
            //Metodo estatico derretir_ultima_fila_si_esta_llena. Como su nombre lo indica, se encarga de corroborar si la ultima fila se encuentra llena y, en caso afirmativo, de borrar todos los copos que en esta se encuentren. 
        static void derretir_ultima_fila_si_esta_llena(List<List<Copo>> matriz, int filas, int cols)
        {
                //Asumo que la ultima fila esta llena. 
            bool completa = true;
                //Recorro la ultima fila buscando una posicion en donde no haya un copo. En caso de encontrarla, marco la variable bandera como falsa, y dejo de recorrer la ultima fila. 
            foreach(Copo c in matriz[filas - 1])
            {
                if(c == null)
                {              
                    completa = false;
                    break;
                }
            }
                //Si la ultima fila resulto estar completa, la recorro reemplazando todos los punteros a los copos que se encontraban en ella por null con el objetivo de eliminarlos.
            if (completa)
            {
                for(int z =0; z < cols; z++)
                {
                    matriz[filas - 1][z] = null;
                }
            }
        }
    }
}
